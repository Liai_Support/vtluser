package com.lia.vtlvehicle.vehicleinfo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.vtlvehicle.view.DashboardActivity;
import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.adapter.VehicleListDetailsAdapter;
import com.lia.vtlvehicle.model.VehicleDetailsBo;
import com.lia.vtlvehicle.utility.ConvertSteam;
import com.lia.vtlvehicle.utility.HttpGetData;
import com.lia.vtlvehicle.utility.Session;
import com.lia.vtlvehicle.utility.StaticInfo;
import com.lia.vtlvehicle.view.LoadingDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class VehicleInventory extends AppCompatActivity implements View.OnClickListener{
    ImageView addVehicle;
    private GridLayoutManager crgm;
    private RecyclerView vehrv;
    private List<VehicleDetailsBo> list_vechDet;
    private ArrayList<VehicleDetailsBo> arraylist;
    private String Url="listVehicleDetailsById";
    private VehicleListDetailsAdapter vechlistadapter;
    private VehicleInventory vehicleInventory;
    private Session session;
    private TextView veditval,vtrashval;
    private String delUrl="deleteVehicleDetail";
    List<VehicleListItem> vehicleListItemList;
    RecyclerView recyclerView;
    private LinearLayout zeroVehicleLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_inventory);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        zeroVehicleLinear = findViewById(R.id.zero_vehicle);
        session = new Session(this);
        this.list_vechDet = list_vechDet;
        vehrv=(RecyclerView)findViewById(R.id.vehicle_make_recy);
        crgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        vehrv.setLayoutManager(crgm);
        JSONObject obj=new JSONObject();
        try {
            obj.put("userid", StaticInfo.userId);
            getvehicleValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        addVehicle = findViewById(R.id.add_vehicle);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_icon));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Intent intent = new Intent(getBaseContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
        addVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VehicleInventory.this,VehicleDetails.class));
            }
        });
    }
    private void getvehicleValues(String response) {
        LoadingDialog loadingDialog=new LoadingDialog(VehicleInventory.this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            Log.d("werty",response);
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        loadingDialog.dismissDialog();
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals("true")){
                            zeroVehicleLinear.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(),"No vehicles in the inventory", Toast.LENGTH_SHORT).show();
                        }
                        else if (obj.getString("error").equals("false")){
                            zeroVehicleLinear.setVisibility(View.GONE);
                            Log.d("json response",""+response);
                            list_vechDet=new ArrayList<>();
                            JSONArray dataArray=obj.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject vechobj = dataArray.getJSONObject(i);
                                VehicleDetailsBo vecdet = new VehicleDetailsBo(vechobj.getInt("id"),vechobj.getString("vehicle_type"),vechobj.getString("vehicle_make"),vechobj.getString("reg_no"),vechobj.getString("insurance_exp_date"),vechobj.getString("fc_exp_date"),vechobj.getString("vehicle_status"),vechobj.getString("permit_type"),String.valueOf(vechobj));
                                list_vechDet.add(vecdet);
                            }
                          Log.d("asdfghj",""+ list_vechDet.size()); ;
                            setupvehicleData(list_vechDet);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                    loadingDialog.dismissDialog();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setupvehicleData(List<VehicleDetailsBo> list_vechDet) {
        vechlistadapter=new VehicleListDetailsAdapter(list_vechDet,this,session,this);
        vehrv.setAdapter(vechlistadapter);
    }

    public void delvechDetails(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+delUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("json response",""+response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true)){
                            Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Intent i=new Intent(getApplicationContext(),VehicleInventory.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
    }

    @SuppressLint("StaticFieldLeak")
    public class VehiclesList extends AsyncTask<String, String, String> {
        private  String EVENT_TAG =VehiclesList.class.getName() ;
        HttpGetData objHttpGetData;
        private String res;
        String URL;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {
            try {
                InputStream inputStream = null;
                URL=arg[0];
                   objHttpGetData = new HttpGetData();
                    inputStream = objHttpGetData.ByGetMethod(URL);
                if (inputStream != null) {
                    ConvertSteam objConvertSteam;
                    objConvertSteam = new ConvertSteam();
                    res = objConvertSteam.ConvertSteamToString(inputStream);
                }
                else {

                }
                if (res.trim().length() == 0) {
                    Log.i(EVENT_TAG, "Check Network" + res);
                }
                Log.i(EVENT_TAG, "Driver Status RESPONSE: " + res);
            }
            catch (Exception e) {
                Log.i(EVENT_TAG, "doInBackground: Exception-- " + e.getMessage());
                e.printStackTrace();
            }
            return res;
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}