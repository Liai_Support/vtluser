package com.lia.vtlvehicle.vehicleinfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.utility.StaticInfo;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class StateAdapter extends ArrayAdapter<Item> {
    private Context mContext;
    private ArrayList<Item> listState;
    private StateAdapter stateAdapter;
    private boolean isFromView = false;
    private ArrayList<String> selectedState = new ArrayList<>();
    public StateAdapter(@NonNull Context context, int resource, List<Item> itemList) {
        super(context, resource,itemList);
        this.mContext = context;
        this.listState = (ArrayList<Item>) itemList;
        this.stateAdapter = this;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView.findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mTextView.setText(listState.get(position).getTitle());
        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).isSelected());
        isFromView = false;
        if ((position <0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        }
        else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }
        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                StringBuilder stringBuilder = new StringBuilder();
                Set<String> sta = new LinkedHashSet<String>();
                if(isChecked){
                    if(isFromView){
                        Item item = listState.get(position);
                        selectedState.add(item.getTitle());
                    }
                    sta = new LinkedHashSet<>(selectedState);
                    for (String state : sta) {
                        stringBuilder.append(state);
                        stringBuilder.append(",");
                    }
                }
                else {
                    Item item = listState.get(position);
                    selectedState.add(item.getTitle());
                    sta = new LinkedHashSet<>(selectedState);
                    for (String state : sta) {
                        stringBuilder.append(state);
                        stringBuilder.append(",");
                    }
                }
                StaticInfo.selectedSta =stringBuilder.toString();
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}
