package com.lia.vtlvehicle.vehicleinfo;

public class VehicleListItem {
    private String vehicleSerNo;
    private String brandName;
    private String vehicleRegNo;
    private String permitType;
    private boolean vehicleStatus;

    public String getVehicleSerNo() {
        return vehicleSerNo;
    }

    public void setVehicleSerNo(String vehicleSerNo) {
        this.vehicleSerNo = vehicleSerNo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getPermitType() {
        return permitType;
    }

    public void setPermitType(String permitType) {
        this.permitType = permitType;
    }

    public boolean isVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(boolean vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public VehicleListItem(){
    }
}
