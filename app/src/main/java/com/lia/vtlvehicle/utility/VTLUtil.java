package com.lia.vtlvehicle.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.vtlvehicle.view.LoadingDialog;
import com.lia.vtlvehicle.view.TripActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import cc.cloudist.acplibrary.ACProgressFlower;

public class VTLUtil  {
    @SuppressLint("StaticFieldLeak")
    private static final VTLUtil ourInstance = new VTLUtil();
    public String currentPhotoPath;
    public Context getContext() {
        return context;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public static Context context;
    public static Pattern aadhaarPattern = Pattern.compile("^[2-9]{1}[0-9]{11}$");
    public static Pattern licencePattern = Pattern.compile("^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$");
    public static Pattern panCardPattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
    public static Pattern voterIdPattern = Pattern.compile("[A-Z]{3}[0-9]{7}");
    public String[] months = new String[]{"Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static String bucket = "vtl-app";
    public static int percentage;
    public String selectedState;
    public AmazonS3 amazonS3;
    public static AmazonS3 s3Client;
    public static TransferUtility transferUtility;
    List<String> listing;
    public static ACProgressFlower progressFlower;
    private VTLUtil() {
    }
    public static VTLUtil getInstance() {
        return ourInstance;
    }
    @SuppressLint("SimpleDateFormat")
    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(imageFileName,  /* prefix */".jpg", /* suffix */ storageDir/* directory */ );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = imageFile.getAbsolutePath();
        return imageFile;
    }
    /*
       Bitmap Convert Base64 String format
      */
    public String bitMapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    public void s3credentialsProvider(){
         CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider = new CognitoCachingCredentialsProvider(
                getContext(),
                "us-east-1:14d94898-d87d-4c7f-b6a4-32f6141f0ffc", // Identity pool ID
                Regions.US_EAST_1 // Region
        );
        createAmazonS3Client(cognitoCachingCredentialsProvider);
    }

    public void createAmazonS3Client(CognitoCachingCredentialsProvider credentialsProvider) {
        amazonS3 = new AmazonS3Client(credentialsProvider);
        amazonS3.setRegion(Region.getRegion(Regions.US_EAST_1));
    }

    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getContext().getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}


