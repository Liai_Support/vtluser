package com.lia.vtlvehicle.utility;

import android.util.Log;

import com.lia.vtlvehicle.model.Article;
import com.lia.vtlvehicle.model.Driver;
import com.lia.vtlvehicle.model.Fowner;

import java.util.ArrayList;

public class StaticInfo {
   public static String MAIN_URL="https://www.vtlpl.com/new_app/service/tech.php?action=";
    public static String NEW_URL="https://www.vtlpl.com/vtlapi/transporter/";
    public static String NEW_URL1="https://www.vtlpl.com/vtlapi/driver/";
    public static String DRIVER_STATUS="driverOnlineStatus";
    public static String DRIVER_UPDATE="updateDriver";
    public static String imgStrBase="";
    public static String imgPathStr="https://vtl-app.s3.amazonaws.com/";
    public static String FLEET_OWNER_REG="fleetOwnerRegister";
    public static String otpvalue="";
    public static boolean credit=false;
    public static ArrayList<Article> listArticle=new ArrayList<Article>();
    public static Article articleList;
    public static Driver driver=new Driver();
    public static Fowner fowner=new Fowner();
    public static Article article=new Article();
    public static int userId=0;
    public static String mobNo="";
    public static String selectedSta;
    public static String getLoginUrl(String mobile, String password){
        String finalUrl=MAIN_URL+"fleetOwnerlogin";
        finalUrl+="&mobile="+mobile;
        finalUrl+="&password="+password;
        return finalUrl;
    }
    public static String checkMobileNumber(String mobileNo){
        String finalURL=MAIN_URL+"fleetownermobilecheck";
        finalURL+="&mobile="+mobileNo;
        return finalURL;
    }
    public static String getchangepswdUrl(int id,String cpswd,String pswd){
        String finalUrl=MAIN_URL+"changeFleetPass";
        finalUrl+="&id="+id;
        finalUrl+="&cpswd="+cpswd;
        finalUrl+="&pswd="+pswd;
        Log.d("jgytydtersfc",""+finalUrl);
        return finalUrl;
    }

    public static String getFownerDetailsUrl(int id){
        String finalUrl=MAIN_URL+"getTechFleetDetailsById";
        finalUrl+="&id="+id;
        return finalUrl;
    }

    public static String getRegisterUrl(String mobileStr, String passwordStr, String statusStr, String imei){
        String finalUrl = MAIN_URL + FLEET_OWNER_REG;
        try {
            finalUrl += "&mobile=" + mobileStr;
            finalUrl += "&password=" + passwordStr;
            finalUrl += "&status=" + statusStr;
            finalUrl += "&imei=" + imei;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return finalUrl;
    }

    public static String getArticleUrl(){
        return MAIN_URL + "listAcArticles";
    }

    public static String getArticleByIdUrl(int id){
        String finalUrl=MAIN_URL+"articleById";
        finalUrl+="&id="+id;
        return finalUrl;
    }
    /*
    Forgot Password
    */
    public static String getForgotPWDUrl(int currentUserID, String password){
        String finalUrl = MAIN_URL + "updateFleetPassword";
        try {
            finalUrl += "&userid=" + currentUserID;
            finalUrl += "&password=" + password;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return finalUrl;
    }
}
