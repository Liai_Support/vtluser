package com.lia.vtlvehicle.utility;

import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpGetData {
   public  InputStream ByGetMethod(String ServerURL) {
        InputStream inputStream = null;
        try {
            URL url = new URL(ServerURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(15000);
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            int response = httpURLConnection.getResponseCode();
            Log.e("Get RESPONSE", "Error in GetData"+httpURLConnection.getResponseMessage());
            if (response == HttpURLConnection.HTTP_OK) {
                inputStream = httpURLConnection.getInputStream();
            }
        }
        catch (Exception e) {
           Log.e("GetData", "Error in GetData", e);
        }
        return inputStream;
    }
}
