package com.lia.vtlvehicle.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.helper.DialogLoading;
import com.lia.vtlvehicle.model.CompletedTripBo;
import com.lia.vtlvehicle.model.OngoingTripBo;
import com.lia.vtlvehicle.utility.Session;
import com.lia.vtlvehicle.utility.StaticInfo;
import com.lia.vtlvehicle.utility.VTLUtil;
import com.lia.vtlvehicle.view.CompletedTrip;

import com.lia.vtlvehicle.view.LoadingDialog;
import com.lia.vtlvehicle.view.OngoingTrip;
import com.lia.vtlvehicle.view.OngoingTrip;
import com.lia.vtlvehicle.view.TripActivity;
import com.lia.vtlvehicle.view.ViewPhoto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OngoingTripAdapter extends RecyclerView.Adapter<OngoingTripAdapter.ViewHolder> {
    List<OngoingTripBo> list_ongoing_trip;
    Context ct;
    private Session session;
    private String m_Text = "";
    private AlertDialog dialog;
    private OngoingTrip ongoingTrip;
    private String updateUrl="updateStatus";
    private String addExpensesUrl="addExpenses";
    private String uploadTripimagesUrl="uploadTripimages";
    private VTLUtil __UTIL = VTLUtil.getInstance();
    public JSONArray unloadpicArray=new JSONArray();
    public   JSONArray loadpicArray=new JSONArray();
    public   JSONArray currentArray=new JSONArray();
    public  String currentPhotoPath,currenttripId;
    public static final int CAMERA_REQUEST_CODE = 102;
    public  boolean unloadedPic,loadedPic,tollChargePic,repairChargePic;
    public OngoingTripAdapter(List<OngoingTripBo> list_ongoing_trip, Context ct, Session session, OngoingTrip ongoingTrip) {
        this.list_ongoing_trip = list_ongoing_trip;
        this.ct = ct;
        this.session = session;
        this.ongoingTrip = ongoingTrip;
    }

    @Override
    public OngoingTripAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ongoing_layout, parent, false);
        return new OngoingTripAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final OngoingTripAdapter.ViewHolder holder, int position) {
        final OngoingTripBo listComptripData = list_ongoing_trip.get(position);
        holder.tripno.setText("Trip ID: " + listComptripData.getTripId());
        holder.fromdate.setText("Date: " + listComptripData.getTripDate());
        holder.fromaddrs.setText(listComptripData.getTripFrom());
        holder.toaddrss.setText(listComptripData.getTripTo());
        holder.tripsal.setText(listComptripData.getTripSal());
        holder.vname.setText(listComptripData.getTripVechName());
        holder.vtype.setText(listComptripData.getTripVechType());
        holder.vregno.setText(listComptripData.getTripVechReg());
        holder.vpermit.setText(listComptripData.getTripVecPermit());
        holder.halttext.setText(listComptripData.getTriphalting());
        holder.othertxt.setText(listComptripData.getTripother());
        holder.haltremark.setText(listComptripData.getTriphaltingrmrk());
        holder.otherremark.setText(listComptripData.getTripotherrmrk());
        if (listComptripData.getTripayStatus().equals("paid") || listComptripData.getTripayStatus().equals("advpaid")) {
            holder.paystatustxt.setText("Paid");
            holder.paystatustxt.setTextColor(ContextCompat.getColor(ct,R.color.green));
        }
        else{
            holder.paystatustxt.setText("UnPaid");
            holder.paystatustxt.setTextColor(ContextCompat.getColor(ct,R.color.red));
        }
        holder.viewmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.viewmore.setVisibility(View.GONE);
                holder.cardlayout.setVisibility(View.VISIBLE);
            }
        });
        holder.viewless.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.viewmore.setVisibility(View.VISIBLE);
                holder.cardlayout.setVisibility(View.GONE);
            }
        });
        holder.acptbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject obj=new JSONObject();
                try {
                    obj.put("tripId", listComptripData.getTripId());
                    obj.put("status", "accepted");
                    getStatusUpdate(String.valueOf(obj));

                    Log.d("statusupdate",""+obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.rejctbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject obj=new JSONObject();
                try {
                    obj.put("tripId", listComptripData.getTripId());
                    obj.put("status", "rejected");
                    getStatusUpdate(String.valueOf(obj));

                    Log.d("statusupdate",""+obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.expandview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.expenseseexpandviewlinear.getVisibility()==View.VISIBLE) {
                    holder.expenseseexpandviewlinear.setVisibility(View.GONE);
                }
                else{
                    holder.expenseseexpandviewlinear.setVisibility(View.VISIBLE);
                }
            }
        });
        holder.unloadviewphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(ct, ViewPhoto.class);
                if (listComptripData.getTripUnloaded()!=null || listComptripData.getTripUnloaded().equals("") || listComptripData.getTripUnloaded()==""){
                    i.putExtra("ImgName",""+listComptripData.getTripUnloaded());
                }
                else{
                    i.putExtra("ImgName","[]");
                }
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ct.startActivity(i);
            }
        });
        holder.loadedviwphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(ct, ViewPhoto.class);
                if (listComptripData.getTripLoaded()!=null || listComptripData.getTripLoaded().equals("") || listComptripData.getTripLoaded()==""){
                    i.putExtra("ImgName",""+listComptripData.getTripLoaded());
                }
                else{
                    i.putExtra("ImgName","[]");
                }
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ct.startActivity(i);
            }
        });
        holder.unloadvechcapt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unloadedPic = true;
                loadedPic = false;
                currenttripId= String.valueOf(listComptripData.getTripId());
                if (listComptripData.getTripUnloaded()!=null || listComptripData.getTripUnloaded().equals("") || listComptripData.getTripUnloaded()=="") {
                    try {
                        JSONArray newarray=new JSONArray(listComptripData.getTripUnloaded());
                        currentArray=newarray;
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    JSONArray newarray=new JSONArray();
                    currentArray=newarray;
                }
                ongoingTrip.takePicture();
            }
        });
        holder.loadvechcapt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadedPic = true;
                unloadedPic = false;
                currenttripId= String.valueOf(listComptripData.getTripId());
                if (listComptripData.getTripLoaded()!=null || listComptripData.getTripLoaded().equals("") || listComptripData.getTripLoaded()=="") {
                    try {
                        JSONArray newarray=new JSONArray(listComptripData.getTripLoaded());
                        currentArray=newarray;
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    JSONArray newarray=new JSONArray();
                    currentArray=newarray;
                }
                ongoingTrip.takePicture();
            }
        });
        holder.savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject addexpensobj=new JSONObject();
                try {
                    addexpensobj.put("tripId",""+listComptripData.getTripId());
                    addexpensobj.put("haltingCharges",holder.halttext.getText().toString());
                    addexpensobj.put("hRemarks",holder.haltremark.getText().toString());
                    addexpensobj.put("otherCharges",holder.othertxt.getText().toString());
                    addexpensobj.put("oRemarks",holder.otherremark.getText().toString());
                    ongoingTrip.addExpense(""+addexpensobj);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void getStatusUpdate(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ct);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("qwerty",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+updateUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("strrrrsdfsd",response);
                    try {
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(ct,"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            Intent i =new Intent(ct, TripActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ct.startActivity(i);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            Log.d("stringRequest",""+stringRequest);
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list_ongoing_trip.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tripno, fromaddrs, toaddrss, fromdate, tripsal, viewmore, viewless,
                vname, vtype, vregno, vpermit,acptbtn,rejctbtn,loadvechcapt,unloadvechcapt,loadedviwphoto,unloadviewphoto,halttext,haltremark
                ,othertxt,otherremark,savebtn,expandview,paystatustxt;
        private LinearLayout blinear,expenseseexpandviewlinear,acceptedbelowlinear,actionbtnLinear;
        private CardView cardlayout,imgcard,expensecard;
        private Spinner expensespinnerbtn;
        public ViewHolder(View itemView) {
            super(itemView);
            tripno = itemView.findViewById(R.id.tripId);
            fromaddrs = itemView.findViewById(R.id.faddrestxt);
            toaddrss = itemView.findViewById(R.id.toaddrstxt);
            fromdate = itemView.findViewById(R.id.fadddate);
            tripsal = itemView.findViewById(R.id.trippayment);
            viewmore = itemView.findViewById(R.id.viewmoretxt);
            viewless = itemView.findViewById(R.id.viewlesstxt);
            paystatustxt = itemView.findViewById(R.id.paystatus);
            vname = itemView.findViewById(R.id.vechnametxt);
            vtype = itemView.findViewById(R.id.vechtypext);
            vregno = itemView.findViewById(R.id.vechregnotxt);
            vpermit = itemView.findViewById(R.id.permittxt);
            cardlayout = itemView.findViewById(R.id.card3);
            acptbtn = itemView.findViewById(R.id.acptbtn);
            rejctbtn = itemView.findViewById(R.id.rejectbtn);
            imgcard = itemView.findViewById(R.id.card4);
            expensecard = itemView.findViewById(R.id.card5);
            loadvechcapt = itemView.findViewById(R.id.takeloadedvecpic);
            unloadvechcapt = itemView.findViewById(R.id.takeunloadvecpic);
            loadedviwphoto = itemView.findViewById(R.id.loadview);
            unloadviewphoto = itemView.findViewById(R.id.unloadview);
            expensespinnerbtn = itemView.findViewById(R.id.expensespinner);
            expandview = itemView.findViewById(R.id.expandview);
            expenseseexpandviewlinear = itemView.findViewById(R.id.expandviewlinear);
            acceptedbelowlinear = itemView.findViewById(R.id.acceptedbelowlinear);
            actionbtnLinear = itemView.findViewById(R.id.actionbtnlinear);
            halttext = itemView.findViewById(R.id.tolltext);
            haltremark = itemView.findViewById(R.id.haltremark);
            othertxt = itemView.findViewById(R.id.repairtext);
            otherremark = itemView.findViewById(R.id.otherchargeremark);
            savebtn = itemView.findViewById(R.id.saveexpend);
        }
    }
}
