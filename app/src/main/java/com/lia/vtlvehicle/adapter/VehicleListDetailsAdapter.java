package com.lia.vtlvehicle.adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.view.UpdateVehicleDetails;
import com.lia.vtlvehicle.model.VehicleDetailsBo;
import com.lia.vtlvehicle.utility.Session;
import com.lia.vtlvehicle.utility.StaticInfo;
import com.lia.vtlvehicle.vehicleinfo.VehicleInventory;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static java.lang.Integer.sum;

public class VehicleListDetailsAdapter extends RecyclerView.Adapter<VehicleListDetailsAdapter.ViewHolder> {
    List<VehicleDetailsBo> vehicleDetailsBos;
    Context ct;
    private Session session;
    private String Url="updatecart";
    private String statusUrl="vehiceAvailableStatus";
    private long lastClickTime = 0;
    private VehicleInventory vehicleInventory;
    HashMap<String, Integer> hashMap = new LinkedHashMap<String, Integer>();
    public VehicleListDetailsAdapter(List<VehicleDetailsBo> vehicleDetailsBos, Context ct, Session session, VehicleInventory vehicleInventory) {
        this.vehicleDetailsBos = vehicleDetailsBos;
        this.ct = ct;
        this.session=session;
        this.vehicleInventory=vehicleInventory;
    }

    @Override
    public VehicleListDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_listitem,parent,false);
        return new VehicleListDetailsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        VehicleDetailsBo vehDetaBo = vehicleDetailsBos.get(position);
        //binding the data with the viewholder views
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.vehSerNoTxt.setText("Vehicle: "+sum(position,1));
        }
        else {
            int pos=position+1;
            holder.vehSerNoTxt.setText("Vehicle: "+pos);
        }
        holder.vehBrandTxt.setText(vehDetaBo.getVehiclemake());
        holder.vehRegNoTxt.setText(vehDetaBo.getRegistNo());
        holder.vehPerTypTxt.setText("Permit Type - "+vehDetaBo.getPermiType());
        if (vehDetaBo.getvStatus().equals("Y")){
            holder.vehicleStsSwitch.setChecked(true);
        }
        else if(vehDetaBo.getvStatus().equals("N")){
            holder.vehicleStsSwitch.setChecked(false);
        }
        holder.trashtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject reqobj = new JSONObject();
                try {
                    reqobj.put("id",vehDetaBo.getId());
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                vehicleInventory.delvechDetails(String.valueOf(reqobj));
            }
        });
        holder.edittxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(ct, UpdateVehicleDetails.class);
                i.putExtra("vDetails",vehDetaBo.getDetails());
                ct.startActivity(i);
            }
        });
        holder.vehicleStsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 String statusString="";
                if (isChecked) {
                    statusString = "Y";
                    JSONObject reqobj = new JSONObject();
                    try {
                        reqobj.put("id",vehDetaBo.getId());
                        reqobj.put("vehicleStatus","Y");
                        vecOnlineStatus(String.valueOf(reqobj));
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    statusString = "N";
                    JSONObject reqobj = new JSONObject();
                    try {
                        reqobj.put("id",vehDetaBo.getId());
                        reqobj.put("vehicleStatus","N");
                        vecOnlineStatus(String.valueOf(reqobj));
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            private void vecOnlineStatus(String response) {
                try {
                    RequestQueue requestQueue = Volley.newRequestQueue(ct);
                    JSONObject jsonBody = new JSONObject(response);
                    Log.d("werty",response);
                    final String requestcartBody = jsonBody.toString();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+statusUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("werty",response);
                            try {
                                JSONObject obj = new JSONObject(response);
                                if(obj.getString("error").equals("true")||obj.getString("error")=="true"){

                                }
                                else if (obj.getString("error").equals("false")||obj.getString("error")=="false"){

                                }

                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("VOLLEY", error.toString());
                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }
                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            10000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
           }

    @Override
    public int getItemCount() {
        return vehicleDetailsBos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img,pimg;
        private TextView txt,productname,size,cstyletxt,qtytxt,Kheematxt,pstyletxt,branchtxt,cpricetxt,plusbtntxt,minusbtntxt,removetxt,qtytext;
        private LinearLayout blinear;
        TextView vehSerNoTxt, vehBrandTxt,vehRegNoTxt,vehPerTypTxt,trashtxt,edittxt;
        Switch vehicleStsSwitch;
        public ViewHolder(View itemView) {
            super(itemView);
            vehSerNoTxt = itemView.findViewById(R.id.vehicle_no);
            vehBrandTxt = itemView.findViewById(R.id.brand_name);
            vehRegNoTxt = itemView.findViewById(R.id.veh_reg_no);
            vehPerTypTxt = itemView.findViewById(R.id.permitype);
            edittxt = itemView.findViewById(R.id.vedit);
            trashtxt = itemView.findViewById(R.id.vtrash);
            vehPerTypTxt = itemView.findViewById(R.id.permitype);
            vehicleStsSwitch = itemView.findViewById(R.id.veh_status);
        }
    }
}