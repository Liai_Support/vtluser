package com.lia.vtlvehicle.view;

import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.utility.StaticInfo;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ChangePassword extends AppCompatActivity {
    EditText curpswd,pswdtxt,cpswdtxt;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        curpswd=(EditText)findViewById(R.id.curpswd);
        pswdtxt=(EditText)findViewById(R.id.pswdtxt);
        cpswdtxt=(EditText)findViewById(R.id.cpswdtxt);
        submit=(Button)findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String curpswdStr = curpswd.getText().toString();
                String passwordStr = pswdtxt.getText().toString();
                String cpasswordStr = cpswdtxt.getText().toString();
             if (!passwordStr.equals(cpasswordStr)){
                 Toast.makeText(getBaseContext(), "Password and confirm Password Doesn't match", Toast.LENGTH_LONG).show();
             }
             else{
                 Log.d("jgytydterxdfc",""+curpswdStr+""+passwordStr);
                 new HttpAsyncTask().execute(StaticInfo.getchangepswdUrl(StaticInfo.userId, curpswdStr, passwordStr));
             }
            }
        });
    }

    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            Log.d("dfghjklsh",result);
            try {
                if(result.equals("0")){
                    Toast.makeText(getBaseContext(), "Invalid ", Toast.LENGTH_LONG).show();
                }
                else if(result.equals("success")){
                    Toast.makeText(getBaseContext(), "Password Changed successfully", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ChangePassword.this, DashboardActivity.class);
                    startActivity(intent);
                }
                else if(result.equals("failure")){
                    Toast.makeText(getBaseContext(), "Current Password Invalid", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}