package com.lia.vtlvehicle.view;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.retrofit.APIUtils;
import com.lia.vtlvehicle.retrofit.contactinfo.ContactInfoAPI;
import com.lia.vtlvehicle.retrofit.contactinfo.ContactInfoService;
import com.lia.vtlvehicle.utility.CustomMethods;
import com.lia.vtlvehicle.utility.Session;
import com.lia.vtlvehicle.utility.StaticInfo;
import com.lia.vtlvehicle.utility.VTLUtil;
import com.lia.vtlvehicle.vehicleinfo.VehicleInventory;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactInfoRegisterActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int CAMERA_REQUEST = 1888;
    TextView adharPicBtn,pancardPicBtn;
    ImageView adharpic,pancardpic;
    EditText firstname,dob,mobno,adharNo,pancardNo;
    RadioGroup relationShip;
    RadioButton  feeOwnRB,feeManRB,otheRB;
    Button cancel,submit;
    private Session session;
    private String adharBaseStr,panBaseStr;
    private String Url="updateFleetOwner";
    private ProgressBar progressBar,panprogressBar;
    private boolean adhar,pancard;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private VTLUtil __UTIL = VTLUtil.getInstance();
    LinearLayout linearLayout1;
    PopupWindow popupWindow;
    private static String adharImgBaseStr, pancardImgStr;
    private File photoFile;
    private Spinner noOfVehicle;
    public TransferUtility transferUtility;
    private ACProgressFlower progressFlower;
    private int percentage;
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            java.text.DecimalFormat nft = new java.text.DecimalFormat("#00.###");
            if (selectedYear <= mYear) {
                dob.setText(nft.format(selectedDay) + "-" + nft.format(selectedMonth + 1) + "-" + selectedYear);
            }
            else {
                dob.setText("");
                Toast.makeText(getBaseContext(), " DOB is invalid", Toast.LENGTH_LONG).show();
            }
        }
    };
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dob:
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR)- CustomMethods.MAX_AGE;
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                showDialog(0);
                break;
            case R.id.takeaadhar:
                adhar = true;
                pancard = false;
                takePicture();
                break;
            case R.id.takepancard:
                adhar = false;
                pancard = true;
                takePicture();
                break;
            case R.id.prevoius:
                break;
            case R.id.submitbtn:
                StaticInfo.fowner.setFullName(firstname.getText().toString().trim());
                StaticInfo.fowner.setDob(dob.getText().toString().trim());
                StaticInfo.fowner.setContactNo(mobno.getText().toString().trim());
                StaticInfo.fowner.setAadhaarNo(adharNo.getText().toString().trim());
                StaticInfo.fowner.setPancardNo(pancardNo.getText().toString().trim());
                if (adharBaseStr!=null){
                    StaticInfo.fowner.setAadhaarImg(adharBaseStr);
                }
                else {
                    StaticInfo.fowner.setAadhaarImg(StaticInfo.fowner.getAadhaarImg());
                }
                if (panBaseStr!=null){
                     StaticInfo.fowner.setPancardImg(panBaseStr);
                }
                else{
                     StaticInfo.fowner.setPancardImg(StaticInfo.fowner.getPancardImg());
                 }
                JSONObject reqobj = new JSONObject();
                try {
                    reqobj.put("id", StaticInfo.userId);
                    reqobj.put("fullName", StaticInfo.fowner.getFullName());
                    reqobj.put("dob", StaticInfo.fowner.getDob());
                    reqobj.put("relationship", StaticInfo.fowner.getRelationship());
                    reqobj.put("aadhaarNo",StaticInfo.fowner.getAadhaarNo());
                    reqobj.put("aadharImg", StaticInfo.fowner.getAadhaarImg());
                    reqobj.put("pancardNo",StaticInfo.fowner.getPancardNo());
                    reqobj.put("pancardImg",StaticInfo.fowner.getPancardImg());
                    reqobj.put("novehicleOwned", 0);
                    if (StaticInfo.fowner.getStatus()!=null || !StaticInfo.fowner.getStatus().equals("")) {
                        reqobj.put("status", StaticInfo.fowner.getStatus());
                    }
                    else {
                        reqobj.put("status", "P");
                    }
                    getContactrequest(reqobj);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.cancel_btn:
                startActivity(new Intent(ContactInfoRegisterActivity.this, DashboardActivity.class));
                break;
            case R.id.feowner_rdb:
                StaticInfo.fowner.setRelationship("Fleet Owner");
              break;
            case R.id.flman_rdb:
                StaticInfo.fowner.setRelationship("Fleet Manager");
                break;
            case R.id.obtn:
                StaticInfo.fowner.setRelationship("Others");
                break;
        }
    }
    private void getContactrequest(JSONObject response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(String.valueOf(response));
            Log.d("request",String.valueOf(response));
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals( "true")){
                            Toast.makeText(getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT).show();
                        }
                        else if (obj.getString("error").equals("false")){
                            Log.d("response",response);
                            Log.d("responsesdfsdfsdf",obj.getString("error"));
                            Toast.makeText(getApplicationContext(),"Successfully Added",Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(ContactInfoRegisterActivity.this,DashboardActivity.class);
                            startActivity(i);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void vehicleInventory() {
        startActivity(new Intent(ContactInfoRegisterActivity.this, VehicleInventory.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactinfo_register);
        session = new Session(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        firstname = findViewById(R.id.fullname);
        dob = findViewById(R.id.dob);
        mobno = findViewById(R.id.mobno);
        relationShip = findViewById(R.id.relationship);
        feeOwnRB = findViewById(R.id.feowner_rdb);
        feeManRB = findViewById(R.id.flman_rdb);
        otheRB = findViewById(R.id.obtn);
        adharNo = findViewById(R.id.adharno);
        adharpic = findViewById(R.id.aadharpic);
        adharPicBtn = findViewById(R.id.takeaadhar);
        pancardNo = findViewById(R.id.pancardno);
        pancardpic = findViewById(R.id.pancardpic);
        pancardPicBtn = findViewById(R.id.takepancard);
        noOfVehicle = findViewById(R.id.novehicle);
        cancel = findViewById(R.id.cancel_btn);
        submit = findViewById(R.id.submitbtn);
        progressBar = findViewById(R.id.adharprogress);
        panprogressBar = findViewById(R.id.panprogress);
        mobno.setText(StaticInfo.fowner.getContactNo());
        firstname.setText(StaticInfo.fowner.getFullName());
        dob.setText(StaticInfo.fowner.getDob());
        adharNo.setText(StaticInfo.fowner.getAadhaarNo());
        pancardNo.setText(StaticInfo.fowner.getPancardNo());
        dob.setKeyListener(null);
        dob.setOnClickListener(this);
        adharPicBtn.setOnClickListener(this);
        pancardPicBtn.setOnClickListener(this);
        submit.setOnClickListener(this);
        cancel.setOnClickListener(this);
        feeOwnRB.setOnClickListener(this);
        feeManRB.setOnClickListener(this);
        otheRB.setOnClickListener(this);
        if (StaticInfo.fowner.getRelationship() != "" || StaticInfo.fowner.getRelationship() != null) {
            Log.d("if", StaticInfo.fowner.getRelationship());
            if (StaticInfo.fowner.getRelationship().equals("Fleet Owner")) {
                feeOwnRB.setChecked(true);
            }
            else if (StaticInfo.fowner.getRelationship().equals("Fleet Manager")) {
                Log.d("if", StaticInfo.fowner.getRelationship());
                feeManRB.setChecked(true);
            }
            else if (StaticInfo.fowner.getRelationship().equals("Others")) {
                otheRB.setChecked(true);
            }
        }
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_icon));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Intent intent = new Intent(getBaseContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
        if (__UTIL.getContext() == null) {
            __UTIL.setContext(this);
        }
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("1-3");
        arrayList.add("3-5");
        arrayList.add("5-7");
        arrayList.add("10");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        noOfVehicle.setAdapter(arrayAdapter);
        if (StaticInfo.fowner.getNoOfVehicleOwned() != null) {
            noOfVehicle.setSelection(arrayList.indexOf(StaticInfo.fowner.getNoOfVehicleOwned()));
        }
        __UTIL.s3credentialsProvider();
        setTransferUtility();
        if (StaticInfo.fowner.getAadhaarImg() != null || !StaticInfo.fowner.getAadhaarImg().equals("")) {
            progressBar.setVisibility(View.VISIBLE);
            adharpic.setVisibility(View.GONE);
            Glide.with(this)
                    .load("https://vtl-app.s3.amazonaws.com/"+StaticInfo.fowner.getAadhaarImg()).placeholder(R.drawable.defaultimg)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            adharpic.setVisibility(View.VISIBLE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            adharpic.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(adharpic);
        }
        if (StaticInfo.fowner.getPancardImg() != null || !StaticInfo.fowner.getPancardImg().equals("")) {
            panprogressBar.setVisibility(View.VISIBLE);
            pancardpic.setVisibility(View.GONE);
            Glide.with(this)
                    .load("https://vtl-app.s3.amazonaws.com/"+StaticInfo.fowner.getPancardImg()).placeholder(R.drawable.defaultimg)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            panprogressBar.setVisibility(View.GONE);
                            pancardpic.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            panprogressBar.setVisibility(View.GONE);
                            pancardpic.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(pancardpic);
           Log.d("panImg","https://vtl-app.s3.amazonaws.com/"+StaticInfo.fowner.getPancardImg());
        }
    }

    private void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            photoFile = null;
            try {
                photoFile = __UTIL.createImageFile();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.lia.vtlvehicle.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, mYear, mMonth, mDay);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            File file = new File(__UTIL.currentPhotoPath);
                if (adhar) {
                    StaticInfo.fowner.setAadhaarImg(file.getName());
                    adharBaseStr=file.getName();
                    Glide.with(this)
                            .load(Uri.fromFile(file))
                            .placeholder(R.drawable.defaultimg)
                            .into(adharpic);
                    uriUploadAWS(file);
                }
                else if (pancard) {
                    StaticInfo.fowner.setPancardImg(file.getName());
                    panBaseStr=file.getName();
                    Glide.with(this)
                            .load(Uri.fromFile(file))
                            .placeholder(R.drawable.defaultimg)
                            .into(pancardpic);
                    uriUploadAWS(file);
                }
        }
    }

    private void uriUploadAWS(File f) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        if (__UTIL.isConnected()) {
            uploadFileToS3(f.getName());

        }
        else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public void setTransferUtility() {
        transferUtility = new TransferUtility(__UTIL.amazonS3, ContactInfoRegisterActivity.this);
    }

    public void uploadFileToS3(String name) {
        File storeDirectory = new File(__UTIL.currentPhotoPath);
        TransferObserver transferObserver = transferUtility.upload(__UTIL.bucket, name, storeDirectory);
        transferObserverListener(transferObserver);
    }

    private void progressFlowerDismiss(ACProgressFlower progressFlower) {
        if (progressFlower.isShowing()) {
            progressFlower.dismiss();
        }
    }

    private void loadingDialog() {
        progressFlower = new ACProgressFlower.Builder(ContactInfoRegisterActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text("Please Wait..")
                .fadeColor(Color.DKGRAY).build();
        progressFlower.show();
    }

    public void transferObserverListener(TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState transferState) {
                switch (transferState) {
                    case IN_PROGRESS:
                        loadingDialog();
                        break;
                    case COMPLETED:
                        progressFlowerDismiss(progressFlower);
                        break;
                    case CANCELED:
                        break;
                    case FAILED:
                        progressFlowerDismiss(progressFlower);
                        break;
                    case PAUSED:
                        break;
                    case WAITING_FOR_NETWORK:
                        break;
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                percentage = (int) (bytesCurrent / bytesTotal * 100);
                Log.d("Loading Perc", "" + percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
                progressFlowerDismiss(progressFlower);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
         class ContactInfoToServer extends AsyncTask<Void,Void,ContactInfoAPI> {
             @Override
             protected ContactInfoAPI doInBackground(Void... voids) {
                 ContactInfoAPI contactInfoAPI = new ContactInfoAPI();
                 ContactInfoService  contactInfoService = APIUtils.getContactInfoService();
                 contactInfoAPI.setId(StaticInfo.driver.getId());
                 contactInfoAPI.setFullName(StaticInfo.driver.getUname());
                 contactInfoAPI.setDob(StaticInfo.driver.getDob());
                 contactInfoAPI.setContactNo(StaticInfo.driver.getContactNo());
                 contactInfoAPI.setRelationship(StaticInfo.driver.getRelationShip());
                 contactInfoAPI.setAadhaarNo(StaticInfo.driver.getAdhar());
                 contactInfoAPI.setAadharImg(StaticInfo.driver.getAadhaarImg());
                 contactInfoAPI.setPancardNo(StaticInfo.driver.getPancard());
                 contactInfoAPI.setPancardImg(StaticInfo.driver.getPanCardImg());
                 contactInfoAPI.setNovehicleOwned(StaticInfo.driver.getNoOfVehicleOwned());
                 Call<ContactInfoAPI> contactInfoAPICall = contactInfoService.insertContactInfoData(contactInfoAPI);
                 contactInfoAPICall.enqueue(new Callback<ContactInfoAPI>() {
                     @Override
                     public void onResponse(@NotNull Call<ContactInfoAPI> call, @NotNull Response<ContactInfoAPI> response) {
                         if (response.isSuccessful()) {
                             System.out.println("<< Contact Info Success Response: >> " + new Gson().toJson(response.body()));
                             Toast.makeText(ContactInfoRegisterActivity.this, "Successfully Saved...", Toast.LENGTH_SHORT).show();
                             vehicleInventory();
                         }
                     }
                     @Override
                     public void onFailure(@NotNull Call<ContactInfoAPI> call, @NotNull Throwable throwable) {
                         System.out.println("<< Contact Info Failure Response: >> ( ERROR: >> " + throwable.getMessage());
                     }
                 });
                 return null;
             }

        @Override
        protected void onPostExecute(ContactInfoAPI contactInfoAPI) {
            super.onPostExecute(contactInfoAPI);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressFlower != null) {
            progressFlower.dismiss();
            progressFlower = null;
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(ContactInfoRegisterActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
