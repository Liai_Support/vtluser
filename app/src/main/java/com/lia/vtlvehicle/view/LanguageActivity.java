package com.lia.vtlvehicle.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.startup.WelcomeActivity;
import com.lia.vtlvehicle.utility.Session;
import com.lia.vtlvehicle.utility.VTLUtil;

import java.util.Locale;

public class LanguageActivity extends AppCompatActivity {
    RadioGroup lanbtn;
    Button btn;
    private Session session;
    String langcode="";
    private VTLUtil __UTIL = VTLUtil.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new Session(this);
        __UTIL.setContext(this);
        langcode=session.getlang();
        Log.d("lang",langcode);
        if (langcode.equals("ta")) {
            setAppLocale("ta");
        }
        else{
            setAppLocale(langcode);
        }
        setContentView(R.layout.activity_language);
        lanbtn = (RadioGroup) findViewById(R.id.radioGroup1);
        RadioButton tamil = (RadioButton) findViewById(R.id.check_tamil);
        RadioButton english = (RadioButton) findViewById(R.id.check_eng);
        btn=(Button)findViewById(R.id.lsubmit);
        if (langcode.equals("eng")){
            english.setChecked(true);
        }
        else {
            tamil.setChecked(true);
        }
        lanbtn.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.check_tamil:
                        // do operations specific to this selection
                        langcode="ta";
                        session.setlang("ta");
                        setAppLocale(langcode);
                        Log.d("tgval", String.valueOf("ta"));
                        Intent intent1 = new Intent(LanguageActivity.this, LanguageActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.check_eng:
                        // do operations specific to this selection
                        langcode="eng";
                        session.setlang("eng");
                        setAppLocale(langcode);
                        Intent intent = new Intent(LanguageActivity.this, LanguageActivity.class);
                        startActivity(intent);
                        Log.d("tgval", String.valueOf("eng"));
                        break;
                }
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LanguageActivity.this, WelcomeActivity.class);
                startActivity(intent);
            }
        });
    }
//    load languages saved in shared preferences
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }
        else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
        editor.putString("My_Lang",localeCode);
        editor.apply();
        res.updateConfiguration(conf,dm);
    }
}
