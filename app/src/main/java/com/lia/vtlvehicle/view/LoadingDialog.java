package com.lia.vtlvehicle.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;

import com.lia.vtlvehicle.R;

public class LoadingDialog {

     private Activity activity;
     private Context context;
     private AlertDialog dialog;
     public LoadingDialog(Activity activity){
         this.activity=activity;
     }

    public void startLoadingDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(activity);
        LayoutInflater inflater=activity.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.custom_dialog,null));
        builder.setCancelable(false);
        dialog=builder.create();
        dialog.show();
        dialog.getWindow().setLayout(500, 500);
     }

    public void dismissDialog(){
         dialog.dismiss();
     }
}
