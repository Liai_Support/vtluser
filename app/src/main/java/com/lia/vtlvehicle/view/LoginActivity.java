package com.lia.vtlvehicle.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.model.Article;
import com.lia.vtlvehicle.model.Fowner;
import com.lia.vtlvehicle.utility.Session;
import com.lia.vtlvehicle.utility.StaticInfo;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class LoginActivity extends AppCompatActivity {
    TextView loginSubmit, register,forget;
    EditText mobile, password;
    CheckBox term;
    private boolean terms;
    private String androidDeviceId;
    private Context context;
    ConnectivityManager connMgr = null;
    TelephonyManager telephonyManager = null;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    LinearLayout linearLayout1;
    private Session session;
    PopupWindow popupWindow;
    private String gender="";
    private String a="";
    private String Url="https://vtlpl.com/app/service/tech.php?action=updateFleetToken";
    private  LoadingDialog loadingDialog;
    public LoginActivity() {
        loadingDialog = new LoadingDialog(LoginActivity.this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new Session(this);
        session.setusename("");
        if (isConnected()) {
            Toast.makeText(LoginActivity.this, "Connected", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(LoginActivity.this, "Not connected", Toast.LENGTH_SHORT).show();
        }
        loginSubmit = (TextView) findViewById(R.id.loginSubmit);
        register = (TextView) findViewById(R.id.signup_here);
        mobile = (EditText) findViewById(R.id.phoneNumber);
        term = (CheckBox) findViewById(R.id.terms_check);
        password = (EditText) findViewById(R.id.pswd);
        forget = (TextView) findViewById(R.id.forget);
        androidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
loginSubmit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        String mobileStr = mobile.getText().toString();
        String passwordStr = password.getText().toString();
        if(mobileStr!=null && passwordStr!=null ){
            Log.d("jkjdcjhgsdfjh",""+StaticInfo.getLoginUrl(mobileStr, passwordStr));
            new HttpAsyncTask().execute(StaticInfo.getLoginUrl(mobileStr, passwordStr));
        }
        else{
            Toast.makeText(getApplicationContext(), "Enter the  details Properly", Toast.LENGTH_SHORT).show();
        }
    }
});
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("dactivity","signup");
                startActivity(intent);
            }
        });
        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("dactivity","forgot");
                startActivity(intent);
            }
        });
        term.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    terms = true;
                }
                else{
                    terms = false;
                }
            }
        });
    }

    private void getInitialValues(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("requesttoken",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("requesttoken",""+response);
                    new HttpAsyncArticleTask().execute(StaticInfo.getArticleUrl());
                    if (response=="success"){
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog.startLoadingDialog();
        }

        @Override
        protected void onPostExecute(String result) {
                loadingDialog.dismissDialog();
            try {
                if(result.equals("0")){
                    Toast.makeText(getBaseContext(), "Invalid Credentials", Toast.LENGTH_LONG).show();
                }
                else if(result.indexOf("success::")!=-1){
                    Log.d("received",result);
                    result=result.replace("success::","");
                    Log.d("received1",result);
                    Toast.makeText(getBaseContext(), "Authentication successfully", Toast.LENGTH_LONG).show();
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Fowner> ownerlist= mapper.readValue(result,new TypeReference<ArrayList<Fowner>>() {});
                    StaticInfo.fowner=  ownerlist.get(0);
                    StaticInfo.userId= StaticInfo.fowner.getId();
                    session.setUserId(StaticInfo.fowner.getId());
                    session.setusename(""+result);
                    String token = FirebaseInstanceId.getInstance().getToken();
                    JSONObject obj=new JSONObject();
                    obj.put("user_id",StaticInfo.userId);
                    obj.put("token",token);
                    getInitialValues(""+obj);
                }
                else{
                    Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class HttpAsyncArticleTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Log.d("receivedghvbn",""+urls);
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("receivedghvbnwxvc",""+result);
                if(result.equals("[]")){
                    Log.d("receivedghvbnwxvc",""+result);
                    loadingDialog.dismissDialog();
                    Intent intent = new Intent(getBaseContext(), DashboardActivity.class);
                    startActivity(intent);
                }
                else{
                    Log.d("receivedghvbnwxvc",""+result);
                    ObjectMapper mapper = new ObjectMapper();
                    StaticInfo.listArticle= mapper.readValue(result,new TypeReference<ArrayList<Article>>() {});
                    StaticInfo.articleList=StaticInfo.listArticle.get(0);
                    loadingDialog.dismissDialog();
                    Intent intent = new Intent(getBaseContext(), DashboardActivity.class);
                    startActivity(intent);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
