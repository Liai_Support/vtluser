package com.lia.vtlvehicle.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.retrofit.APIUtils;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehicleMakeAPI;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehicleMakeService;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehiclePermitTypeAPI;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehiclePermitTypeService;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehicleTypeAPI;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehicleTypeService;
import com.lia.vtlvehicle.utility.StaticInfo;
import com.lia.vtlvehicle.utility.VTLUtil;
import com.lia.vtlvehicle.vehicleinfo.VehicleInventory;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateVehicleDetails extends AppCompatActivity implements View.OnClickListener {
    private VTLUtil __UTIL = VTLUtil.getInstance();
    private VehicleTypeService vehicleTypeService;
    private VehicleMakeService vehicleMakeService;
    private VehiclePermitTypeService vehiclePermitTypeService;
    private Spinner vehTypeSpin, vehMakeSpin, vehPermitTypeSpin;
    private EditText regNoStateEd, regNoStatNoEd, regNoSerEd, regNoVehNoEd, insuExpDateEd, fcExpDateEd;
    private int mYear, mcyear, mMonth, mDay;
    private boolean insurCal, fcCal;
    private ArrayList<String> stateList = new ArrayList<>();
    private SparseBooleanArray sparseBooleanArray;
    private String vechDetId;
    private String Url = "updateVehicleDetail";
    private ArrayList<String> vehicleTypeList = new ArrayList<>();
    private String vehMake, vehType, vehPermit;
    private ListView listview;
    private TextView textView;
    private String selState;
    private TextView selectedStaTxt;
    private int[] textViews = new int[]{R.id.takerc, R.id.takeinsurance, R.id.takefc};
    private File photoFile;
    public static final int CAMERA_REQUEST = 1888;
    private boolean rcPic, insurPic, fcPic;
    private ImageView rcImg, insurImg, fcImg;
    public TransferUtility transferUtility;
    private ACProgressFlower progressFlower;
    private int percentage;
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @SuppressLint("SetTextI18n")
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            java.text.DecimalFormat nft = new java.text.DecimalFormat("#00.###");
            if (selectedYear <= mYear && selectedYear > mcyear) {
                if (insurCal) {
                    insuExpDateEd.setText(nft.format(selectedDay) + "-" + nft.format(selectedMonth + 1) + "-" + selectedYear);
                }
                else if (fcCal) {
                    fcExpDateEd.setText(nft.format(selectedDay) + "-" + nft.format(selectedMonth + 1) + "-" + selectedYear);
                }
            }
            else {
                if (insurCal) {
                    insuExpDateEd.setText("");
                }
                else if (fcCal) {
                    fcExpDateEd.setText("");
                }
                Toast.makeText(getBaseContext(), "Invalid Expiry Date", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_vehicle_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        vehicleTypeService = APIUtils.getVehicleTypeService();
        vehicleMakeService = APIUtils.getVehicleMakeService();
        vehiclePermitTypeService = APIUtils.getVehiclePermitTypeService();
        vehTypeSpin = findViewById(R.id.vehicle_type);
        vehMakeSpin = findViewById(R.id.vehicle_make);
        vehPermitTypeSpin = findViewById(R.id.permit_type);
        regNoStateEd = findViewById(R.id.st_edt);
        regNoStatNoEd = findViewById(R.id.st_no_edt);
        regNoSerEd = findViewById(R.id.ser_no_edt);
        regNoVehNoEd = findViewById(R.id.veh_no_edt);
        insuExpDateEd = findViewById(R.id.insu_date);
        fcExpDateEd = findViewById(R.id.fc_date);
        textView = findViewById(R.id.state_txt);
        selectedStaTxt = findViewById(R.id.selected_state);
        rcImg = findViewById(R.id.rcpic);
        insurImg = findViewById(R.id.insurancepic);
        fcImg = findViewById(R.id.fcpic);
         /*
          Take Camera  TextView OnClick Listener Added.
         */
        for (int textViewId : textViews) {
            TextView textView = findViewById(textViewId);
            textView.setOnClickListener(this);
        }
        Button submitBtn = findViewById(R.id.submitbtn);
        insuExpDateEd.setOnClickListener(this);
        insuExpDateEd.setKeyListener(null);
        fcExpDateEd.setOnClickListener(this);
        fcExpDateEd.setKeyListener(null);
        textView.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_icon));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
        new VehicleInfo().execute();
        String vecDetails = getIntent().getStringExtra("vDetails");
        try {
            JSONObject reqobj = new JSONObject(vecDetails);
            insuExpDateEd.setText(reqobj.getString("insurance_exp_date"));
            fcExpDateEd.setText(reqobj.getString("fc_exp_date"));
            String reg = reqobj.getString("reg_no");
            String[] splitStr = reg.split("\\s+");
            try{
                regNoStateEd.setText(splitStr[0]);
                regNoStatNoEd.setText(splitStr[1]);
                regNoSerEd.setText(splitStr[2]);
                regNoVehNoEd.setText(splitStr[3]);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            vechDetId = reqobj.getString("id");
            vehMake = reqobj.getString("vehicle_make");
            vehType = reqobj.getString("vehicle_type");
            vehPermit = reqobj.getString("permit_type");
            __UTIL.selectedState = vehPermit;
            selectedStaTxt.setText(__UTIL.selectedState);
            StaticInfo.selectedSta = __UTIL.selectedState;
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        __UTIL.s3credentialsProvider();
        setTransferUtility();
        /*
         Image Load from API
         */
        if (StaticInfo.driver.getRcImg() != null) {
            Picasso.get().load("http://vtlpl.com/app/upload/" + StaticInfo.driver.getRcImg()).placeholder(R.drawable.defaultimg).error(R.drawable.defaultimg).into(rcImg);
        }
        else if (StaticInfo.driver.getInsuImg() != null) {
            Picasso.get().load("http://vtlpl.com/app/upload/" + StaticInfo.driver.getInsuImg()).placeholder(R.drawable.ic_otp).error(R.drawable.ic_otp).into(insurImg);
        }
        else if (StaticInfo.driver.getFcImg() != null) {
            Picasso.get().load("http://vtlpl.com/app/upload/" + StaticInfo.driver.getFcImg()).placeholder(R.drawable.ic_otp).error(R.drawable.ic_otp).into(fcImg);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.insu_date:
                insurCal = true;
                fcCal = false;
                calenderDialog();
                break;
            case R.id.fc_date:
                fcCal = true;
                insurCal = false;
                calenderDialog();
                break;
            case R.id.state_txt:
                dialogPopup();
                break;
            case R.id.takerc:
                rcPic = true;
                insurPic = false;
                fcPic = false;
                takePicture();
                break;
            case R.id.takeinsurance:
                rcPic = false;
                insurPic = true;
                fcPic = false;
                takePicture();
                break;
            case R.id.takefc:
                rcPic = false;
                insurPic = false;
                fcPic = true;
                takePicture();
                break;
            case R.id.submitbtn:
                String regNo, state = "", stateNo = "", serial = "", vehNo = "", insuExpDate = "", fcExpDate = "";
                if (regNoStateEd.getText().toString().trim().length() != 0) {
                    state = regNoStateEd.getText().toString().trim();

                }
                else {
                    regNoStateEd.setError("Field cannot empty");
                }
                if (regNoStatNoEd.getText().toString().trim().length() != 0) {
                    stateNo = "  " + regNoStatNoEd.getText().toString().trim();
                }
                else {
                    regNoStatNoEd.setError("Field cannot empty");
                }
                if (regNoSerEd.getText().toString().trim().length() != 0) {
                    serial = "  " + regNoSerEd.getText().toString().trim();
                }
                else {
                    regNoSerEd.setError("Field cannot empty");
                }
                if (regNoVehNoEd.getText().toString().trim().length() != 0) {
                    vehNo = "  " + regNoVehNoEd.getText().toString().trim();
                }
                else {
                    regNoVehNoEd.setError("Field cannot empty");
                }
                if (insuExpDateEd.getText().toString().length() != 0) {
                    insuExpDate = "  " + insuExpDateEd.getText().toString().trim();
                }
                else {
                    insuExpDateEd.setError("Field cannot empty");
                }
                if (fcExpDateEd.getText().toString().length() != 0) {
                    fcExpDate = "  " + fcExpDateEd.getText().toString().trim();
                }
                else {
                    fcExpDateEd.setError("Field cannot empty");
                }
                regNo = state + stateNo + serial + vehNo;
                if (StaticInfo.selectedSta==null || StaticInfo.selectedSta.equals("") ){
                    Toast.makeText(this,"Choose Permit Type",Toast.LENGTH_LONG).show();
                }
                else {
                    JSONObject reqobj = new JSONObject();
                    try {
                        reqobj.put("id", vechDetId);
                        reqobj.put("vehicleType", vehTypeSpin.getSelectedItem().toString());
                        reqobj.put("vehicleMake", vehMakeSpin.getSelectedItem().toString());
                        reqobj.put("regNo", regNo);
                        reqobj.put("permitType", StaticInfo.selectedSta);
                        reqobj.put("regCertificateImg", StaticInfo.driver.getRcImg());
                        reqobj.put("insurexpDate", insuExpDate);
                        reqobj.put("insurImg", StaticInfo.driver.getInsuImg());
                        reqobj.put("fcexpDate", fcExpDate);
                        reqobj.put("fcImg", StaticInfo.driver.getFcImg());
                        reqobj.put("vstatus", "Y");
                        updateVecDetailrequest(String.valueOf(reqobj));
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private void dialogPopup() {
        final Dialog dialog = new Dialog(UpdateVehicleDetails.this);
        dialog.setContentView(R.layout.statelist_dialog);
        dialog.setTitle("Select your Permit area");
        listview = dialog.findViewById(R.id.listView);
        new StateInfo().execute();
        Button dialogButton = dialog.findViewById(R.id.select_state_btn);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedStaTxt.setText("Selected State: " + selState);
                StaticInfo.selectedSta = selState;
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void updateVecDetailrequest(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            Log.d("json request", "" + jsonBody);
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL + Url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject obj = new JSONObject(response);
                        if (obj.getString("error").equals("true") || obj.getString("error") == "true") {
                            Toast.makeText(getBaseContext(), "Oops Error while fetch data", Toast.LENGTH_SHORT).show();
                        }
                        else if (obj.getString("error").equals("false") || obj.getString("error") == "false") {
                            Log.d("responsesdfsdfsdf", obj.getString("error"));
                            Toast.makeText(getApplicationContext(), "Successfully Updated", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(UpdateVehicleDetails.this, VehicleInventory.class));
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void calenderDialog() {
        final Calendar c = Calendar.getInstance();
        mcyear = c.get(Calendar.YEAR);
        mYear = c.get(Calendar.YEAR) + 10;
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        showDialog(0);
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, mYear, mMonth, mDay);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            File file = new File(__UTIL.currentPhotoPath);
            if (rcPic) {
                rcImg.setImageURI(Uri.fromFile(file));
                StaticInfo.driver.setRcImg(file.getName());
                uriUploadAWS(file);
            }
            else if (insurPic) {
                insurImg.setImageURI(Uri.fromFile(file));
                StaticInfo.driver.setInsuImg(file.getName());
                uriUploadAWS(file);
            }
            else if (fcPic) {
                fcImg.setImageURI(Uri.fromFile(file));
                StaticInfo.driver.setFcImg(file.getName());
                uriUploadAWS(file);
            }
        }
    }

    private void uriUploadAWS(File f) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        if (__UTIL.isConnected()) {
            uploadFileToS3(f.getName());
        }
        else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            photoFile = null;
            try {
                photoFile = __UTIL.createImageFile();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.lia.vtlvehicle.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    public void setTransferUtility() {
        transferUtility = new TransferUtility(__UTIL.amazonS3, UpdateVehicleDetails.this);
    }

    public void uploadFileToS3(String name) {
        File storeDirectory = new File(__UTIL.currentPhotoPath);
        TransferObserver transferObserver = transferUtility.upload(__UTIL.bucket, name, storeDirectory);
        transferObserverListener(transferObserver);
    }

    private void progressFlowerDismiss(ACProgressFlower progressFlower) {
        if (progressFlower.isShowing()) {
            progressFlower.dismiss();
        }
    }

    private void loadingDialog() {
        progressFlower = new ACProgressFlower.Builder(UpdateVehicleDetails.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text("Please Wait..")
                .fadeColor(Color.DKGRAY).build();

        progressFlower.show();
    }

    public void transferObserverListener(TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState transferState) {
                switch (transferState) {
                    case IN_PROGRESS:
                        loadingDialog();
                        break;
                    case COMPLETED:
                        progressFlowerDismiss(progressFlower);
                        break;
                    case CANCELED:
                        break;
                    case FAILED:
                        progressFlowerDismiss(progressFlower);
                        break;
                    case PAUSED:
                        break;
                    case WAITING_FOR_NETWORK:
                        break;
                }
            }
            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                percentage = (int) (bytesCurrent / bytesTotal * 100);
                Log.d("Loading Perc", "" + percentage);

            }
            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
                progressFlowerDismiss(progressFlower);

            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class VehicleInfo extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            Call<List<VehicleTypeAPI>> callVehicleType = vehicleTypeService.getAllVehicleType();
            callVehicleType.enqueue(new Callback<List<VehicleTypeAPI>>() {
                @Override
                public void onResponse(@NotNull Call<List<VehicleTypeAPI>> call, @NotNull Response<List<VehicleTypeAPI>> response) {
                    if (response.isSuccessful()) {
                        JSONArray jsonArray = null;
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonArray = new JSONArray(new Gson().toJson(response.body()));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                vehicleTypeList.add(jsonObject.getString("vehicle_type"));
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        vehTypeSpin.setAdapter(new ArrayAdapter<>(UpdateVehicleDetails.this, android.R.layout.simple_spinner_dropdown_item, vehicleTypeList));
                        vehTypeSpin.setSelection(vehicleTypeList.indexOf(vehType));
                        Log.d("dfgh", "" + vehicleTypeList.size());
                    }
                }
                @Override
                public void onFailure(@NotNull Call<List<VehicleTypeAPI>> call, @NotNull Throwable throwable) {
                    Toast.makeText(UpdateVehicleDetails.this, "Vehicle Type Failure:" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            Call<List<VehicleMakeAPI>> callVehicleMake = vehicleMakeService.getAllVehicleMake();
            callVehicleMake.enqueue(new Callback<List<VehicleMakeAPI>>() {
                @Override
                public void onResponse(@NotNull Call<List<VehicleMakeAPI>> call, @NotNull Response<List<VehicleMakeAPI>> response) {
                    if (response.isSuccessful()) {
                        JSONArray jsonArray = null;
                        JSONObject jsonObject = new JSONObject();
                        ArrayList<String> vehicleMakeList = new ArrayList<>();
                        try {
                            jsonArray = new JSONArray(new Gson().toJson(response.body()));

                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                vehicleMakeList.add(jsonObject.getString("vehicles_make"));
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        vehMakeSpin.setAdapter(new ArrayAdapter<>(UpdateVehicleDetails.this, android.R.layout.simple_spinner_dropdown_item, vehicleMakeList));
                        vehMakeSpin.setSelection(vehicleMakeList.indexOf(vehMake));
                    }
                }
                @Override
                public void onFailure(@NotNull Call<List<VehicleMakeAPI>> call, @NotNull Throwable throwable) {
                    Toast.makeText(UpdateVehicleDetails.this, "Vehicle Make Failure: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class StateInfo extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            Call<List<VehiclePermitTypeAPI>> callVehiclePermitTypeService = vehiclePermitTypeService.getAllVehiclePermitType();
            callVehiclePermitTypeService.enqueue(new Callback<List<VehiclePermitTypeAPI>>() {
                @Override
                public void onResponse(@NotNull Call<List<VehiclePermitTypeAPI>> call, @NotNull Response<List<VehiclePermitTypeAPI>> response) {
                    if (response.isSuccessful()) {
                        JSONArray jsonArray = null;
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonArray = new JSONArray(new Gson().toJson(response.body()));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                stateList.add(jsonObject.getString("state_name"));
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(UpdateVehicleDetails.this,
                                    android.R.layout.simple_list_item_multiple_choice,
                                    android.R.id.text1, stateList);
                            listview.setAdapter(adapter);
                            if (vehPermit != null) {
                                String[] permitState = vehPermit.split(",");
                                int state = 0;
                                for (String str : permitState) {
                                    state = stateList.indexOf(str);
                                    System.out.println("SELECTED STATEE" + state);
                                    listview.setItemChecked(state, true);
                                }
                            }
                            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    sparseBooleanArray = listview.getCheckedItemPositions();
                                    selState = "";
                                    int i = 0;
                                    while (i < sparseBooleanArray.size()) {
                                        if (sparseBooleanArray.valueAt(i)) {
                                            selState += stateList.get(sparseBooleanArray.keyAt(i)) + ",";
                                        }
                                        i++;
                                    }
                                    selState = selState.replaceAll("(,)*$", "");
                                }
                            });
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void onFailure(@NotNull Call<List<VehiclePermitTypeAPI>> call, @NotNull Throwable throwable) {
                    Toast.makeText(UpdateVehicleDetails.this, "VehiclePermitTypeService Failure: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            return selState;
        }
        @Override
        protected void onPostExecute(String selectedState) {
            super.onPostExecute(selectedState);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }
}