package com.lia.vtlvehicle.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.vtlvehicle.R;
import com.lia.vtlvehicle.adapter.CompletedTripAdapter;
import com.lia.vtlvehicle.adapter.OngoingTripAdapter;
import com.lia.vtlvehicle.model.CompletedTripBo;
import com.lia.vtlvehicle.model.OngoingTripBo;
import com.lia.vtlvehicle.utility.Session;
import com.lia.vtlvehicle.utility.StaticInfo;
import com.lia.vtlvehicle.utility.VTLUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

import static android.app.Activity.RESULT_OK;

public class OngoingTrip extends Fragment  {
    private static final String Url = "ongoingTrip";
    private static final String statusUrl = "updatestatus";
    int dummyColor;
    private Session session;
    private  TripActivity tripActivity;
    private List<OngoingTripBo> list_oingoing_trip;
    private OngoingTripAdapter ongoingTripAdapter;
    private GridLayoutManager brgm;
    private RecyclerView ongoingtriprv;
    private String updateUrl="updateStatus";
    private String addExpensesUrl="updateExpenses";
    private String uploadTripimagesUrl="uploadTripimages";
    private VTLUtil __UTIL = VTLUtil.getInstance();
    public static final int CAMERA_REQUEST_CODE = 102;
    public TransferUtility transferUtility;
    List<String> listing;
    private int percentage;
    private ACProgressFlower progressFlower;
    String currentPhotoPath;
    private Context ctx;
    public OngoingTrip() {
    }
    @SuppressLint("ValidFragment")
    public OngoingTrip(int color) {
        this.dummyColor = color;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_ongoing_trip, container, false);
        if(__UTIL.getContext()==null){
            __UTIL.setContext(getActivity());
        }
        ongoingtriprv=view.findViewById(R.id.ongoing_rc);
        brgm=new GridLayoutManager(getActivity().getBaseContext(),1,GridLayoutManager.VERTICAL, false);
        ongoingtriprv.setLayoutManager(brgm);
        JSONObject obj=new JSONObject();
        try {
            obj.put("ownerId", StaticInfo.userId);
            getInitialValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        // callback method to call credentialsProvider method.
        __UTIL.s3credentialsProvider();
        // callback method to call the setTransferUtility method
        setTransferUtility();
        return view;
    }

    public String convertStandardJSONString(String data_json){
        data_json = data_json.replace("\"[", "[");
        data_json = data_json.replace("]\"", "]");
        return data_json;
    }

    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(convertStandardJSONString(response));
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            list_oingoing_trip=new ArrayList<>();
                            JSONArray compArray=obj.getJSONArray("data");
                            for (int i = 0; i < compArray.length(); i++) {
                                JSONObject comtripobj = compArray.getJSONObject(i);
                                OngoingTripBo ongoingTripBo = new OngoingTripBo(comtripobj.getInt("tripId"),comtripobj.getString("movementDate"),comtripobj.getString("tFromcity"),
                                        comtripobj.getString("tTocity"),comtripobj.getString("hireAmount"),comtripobj.getString("transporterName"),comtripobj.getString("vType")
                                        ,comtripobj.getString("regNo"),comtripobj.getString("pertmitType"),comtripobj.getString("status"),comtripobj.getString("unloaded"),comtripobj.getString("loaded")
                                ,comtripobj.getString("halting_charges"),comtripobj.getString("halting_remarks"),comtripobj.getString("other_charges"),comtripobj.getString("other_remarks"),comtripobj.getString("hpStatus"));
                                list_oingoing_trip.add(ongoingTripBo);
                            }
                            setupCOrderData(list_oingoing_trip);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = __UTIL.createImageFile();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(), "com.lia.vtlvehicle.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        JSONObject unloadpicObj=new JSONObject();
        JSONObject loadpicObj=new JSONObject();
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            File file = new File(__UTIL.currentPhotoPath);
            if (ongoingTripAdapter.unloadedPic) {
                try {
                    unloadpicObj.put("imgName",file.getName());
                    ongoingTripAdapter.currentArray.put(unloadpicObj);
                    int len=ongoingTripAdapter.currentArray.length();
                    JSONObject imguploadobj=new JSONObject();
                    imguploadobj.put("tripId",ongoingTripAdapter.currenttripId);
                    imguploadobj.put("loaded","");
                    imguploadobj.put("unloaded",""+ongoingTripAdapter.currentArray);
                    uploadTripimg(""+imguploadobj);
                    ongoingTripAdapter.unloadpicArray.remove(0);
                    ongoingTripAdapter.currentArray.remove(0);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                uriUploadAWS(file);
            }
            else if (ongoingTripAdapter.loadedPic) {
                try {
                    unloadpicObj.put("imgName",file.getName());
                    ongoingTripAdapter.currentArray.put(unloadpicObj);
                    int len=ongoingTripAdapter.currentArray.length();
                    JSONObject imguploadobj=new JSONObject();
                    imguploadobj.put("tripId",ongoingTripAdapter.currenttripId);
                    imguploadobj.put("loaded",""+ongoingTripAdapter.currentArray);
                    imguploadobj.put("unloaded","");
                    uploadTripimg(""+imguploadobj);
                    ongoingTripAdapter.loadpicArray.remove(0);
                    ongoingTripAdapter.currentArray.remove(0);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                uriUploadAWS(file);
            }
        }
    }
    private void uploadTripimg(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("qwerty",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+uploadTripimagesUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            Intent i =new Intent(getActivity(),TripActivity.class);
                            startActivity(i);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            Log.d("stringRequest",""+stringRequest);
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void addExpense(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("qwerty",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+addExpensesUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("strrrrsdfsd",response);
                    try {
                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            Intent i =new Intent(getActivity(),TripActivity.class);
                            startActivity(i);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            Log.d("stringRequest",""+stringRequest);
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void uriUploadAWS(File f) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
        if (__UTIL.isConnected()) {
            uploadFileToS3(f.getName());
        }
        else {
            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public void setTransferUtility() {
        transferUtility = new TransferUtility(__UTIL.amazonS3, getActivity());
    }

    public void uploadFileToS3(String name) {
        File storeDirectory = new File(__UTIL.currentPhotoPath);
        TransferObserver transferObserver = transferUtility.upload(__UTIL.bucket, name, storeDirectory);
        transferObserverListener(transferObserver);
    }

    private void setupCOrderData(List<OngoingTripBo> list_oingoing_trip) {
        ongoingTripAdapter=new OngoingTripAdapter(list_oingoing_trip,getActivity().getBaseContext(),session,this);
        ongoingtriprv.setAdapter(ongoingTripAdapter);
    }

    private void progressFlowerDismiss(ACProgressFlower progressFlower) {
        if (progressFlower.isShowing()) {
            progressFlower.dismiss();
        }
    }

    private void loadingDialog() {
        progressFlower = new ACProgressFlower.Builder(getActivity())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text("Please Wait..")
                .fadeColor(Color.DKGRAY).build();

        progressFlower.show();
    }

    public void transferObserverListener(TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState transferState) {
                Log.d("statechange",""+transferState);
                switch (transferState) {
                    case IN_PROGRESS:
                        loadingDialog();
                        break;
                    case COMPLETED:
                        progressFlowerDismiss(progressFlower);
                        break;
                    case CANCELED:
                        break;
                    case FAILED:
                        progressFlowerDismiss(progressFlower);
                        break;
                    case PAUSED:
                        break;
                    case WAITING_FOR_NETWORK:
                        break;
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                percentage = (int) (bytesCurrent / bytesTotal * 100);
                Log.d("Loading Perc", "" + percentage);

            }
            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", ""+ex);
                progressFlowerDismiss(progressFlower);

            }
        });
    }
}