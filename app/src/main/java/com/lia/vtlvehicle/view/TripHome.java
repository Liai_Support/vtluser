package com.lia.vtlvehicle.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.lia.vtlvehicle.R;

public class TripHome extends AppCompatActivity implements  View.OnClickListener {
    private CardView tripcardview,paymentcardview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_home);
        tripcardview=(CardView)findViewById(R.id.tripcard);
        paymentcardview=(CardView)findViewById(R.id.paymencard);
        tripcardview.setOnClickListener(this);
        paymentcardview.setOnClickListener(this);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
    @Override
    public void onClick(View view) {
        if (view==tripcardview){
            Intent i=new Intent(this,TripActivity.class);
            startActivity(i);
        }
        else if (view==paymentcardview){
            Intent i=new Intent(this,TransactionActivity.class);
            startActivity(i);
        }
    }
}