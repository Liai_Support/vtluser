package com.lia.vtlvehicle.retrofit.vehicleinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleTypeAPI {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
}
