package com.lia.vtlvehicle.retrofit.vehicleinfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface VehicleTypeService {
    @GET("tech.php?action=vehicleType")
    Call<List<VehicleTypeAPI>> getAllVehicleType();
}
