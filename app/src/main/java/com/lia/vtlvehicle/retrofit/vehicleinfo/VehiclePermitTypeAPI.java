package com.lia.vtlvehicle.retrofit.vehicleinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehiclePermitTypeAPI {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("state_name")
    @Expose
    private String stateName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }


}
