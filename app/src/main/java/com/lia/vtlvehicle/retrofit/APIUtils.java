package com.lia.vtlvehicle.retrofit;

import com.lia.vtlvehicle.retrofit.contactinfo.ContactInfoService;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehicleInfoService;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehicleMakeService;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehiclePermitTypeService;
import com.lia.vtlvehicle.retrofit.vehicleinfo.VehicleTypeService;

public class APIUtils {
    private static final String API_URL = "https://www.vtlpl.com/new_app/service/";
    private APIUtils() {
    }

    public static ContactInfoService getContactInfoService(){
        return RetrofitClient.getClient(API_URL).create(ContactInfoService.class);
    }

    public static VehicleInfoService getVehicleInfoService(){
        return RetrofitClient.getClient(API_URL).create(VehicleInfoService.class);
    }

    public static VehicleTypeService getVehicleTypeService(){
        return RetrofitClient.getClient(API_URL).create(VehicleTypeService.class);
    }

    public static VehicleMakeService getVehicleMakeService(){
        return RetrofitClient.getClient(API_URL).create(VehicleMakeService.class);
    }

    public static VehiclePermitTypeService getVehiclePermitTypeService(){
        return RetrofitClient.getClient(API_URL).create(VehiclePermitTypeService.class);
    }
}
