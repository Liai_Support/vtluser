package com.lia.vtlvehicle.retrofit.contactinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactInfoAPI {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("fullName")
    @Expose
    private String fullName;

    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("contactNo")
    @Expose
    private String contactNo;

    @SerializedName("relationship")
    @Expose
    private String relationship;

    @SerializedName("aadhaarNo")
    @Expose
    private String aadhaarNo;

    @SerializedName("aadharImg")
    @Expose
    private String aadharImg;

    @SerializedName("pancardNo")
    @Expose
    private String pancardNo;

    @SerializedName("pancardImg")
    @Expose
    private String pancardImg;

    @SerializedName("novehicleOwned")
    @Expose
    private String novehicleOwned;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getAadharImg() {
        return aadharImg;
    }

    public void setAadharImg(String aadharImg) {
        this.aadharImg = aadharImg;
    }

    public String getPancardNo() {
        return pancardNo;
    }

    public void setPancardNo(String pancardNo) {
        this.pancardNo = pancardNo;
    }

    public String getPancardImg() {
        return pancardImg;
    }

    public void setPancardImg(String pancardImg) {
        this.pancardImg = pancardImg;
    }

    public String getNovehicleOwned() {
        return novehicleOwned;
    }

    public void setNovehicleOwned(String novehicleOwned) {
        this.novehicleOwned = novehicleOwned;
    }
}
