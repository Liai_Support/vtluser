package com.lia.vtlvehicle.retrofit.vehicleinfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface VehiclePermitTypeService {
    @GET("tech.php?action=listPermit")
    Call<List<VehiclePermitTypeAPI>> getAllVehiclePermitType();
}
