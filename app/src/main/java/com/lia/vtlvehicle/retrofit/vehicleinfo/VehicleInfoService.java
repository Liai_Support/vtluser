package com.lia.vtlvehicle.retrofit.vehicleinfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface VehicleInfoService {

    @GET("vehicleInfo/")
    Call<List<VehicleInfoAPI>> getAllVehicleInfo();

    @POST("tech.php?action=vehiceDetailsNew")
    Call<VehicleInfoAPI> insertVehicleInfoData(@Body VehicleInfoAPI vehicleInfoAPI);

    @PUT("vehicleInfo/{id}")
    Call<VehicleInfoAPI> updateVehicleInfo(@Path("id") int id, @Body VehicleInfoAPI vehicleInfoAPI);

    @DELETE("vehicleInfo/{id}")
    Call<VehicleInfoAPI> deleteVehicleInfo(@Path("id") int id);
}
