package com.lia.vtlvehicle.retrofit.vehicleinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleMakeAPI {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("vehicles_make")
    @Expose
    private String vehiclesMake;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVehiclesMake() {
        return vehiclesMake;
    }

    public void setVehiclesMake(String vehiclesMake) {
        this.vehiclesMake = vehiclesMake;
    }


}
