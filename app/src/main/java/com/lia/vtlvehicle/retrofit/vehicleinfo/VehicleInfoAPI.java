package com.lia.vtlvehicle.retrofit.vehicleinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleInfoAPI {

    @SerializedName("userid")
    @Expose
    private int userid;

    @SerializedName("vehicleNo")
    @Expose
    private String vehicleNo;

    @SerializedName("vehicleType")
    @Expose
    private String vehicleType;

    @SerializedName("vehicleMake")
    @Expose
    private String vehicleMake;

    @SerializedName("regNo")
    @Expose
    private String regNo;

    @SerializedName("regCertificateImg")
    @Expose
    private String regCertificateImg;

    @SerializedName("insurexpDate")
    @Expose
    private String insurexpDate;

    @SerializedName("insurImg")
    @Expose
    private String insurImg;

    @SerializedName("fcexpDate")
    @Expose
    private String fcexpDate;

    @SerializedName("fcImg")
    @Expose
    private String fcImg;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getRegCertificateImg() {
        return regCertificateImg;
    }

    public void setRegCertificateImg(String regCertificateImg) {
        this.regCertificateImg = regCertificateImg;
    }

    public String getInsurexpDate() {
        return insurexpDate;
    }

    public void setInsurexpDate(String insurexpDate) {
        this.insurexpDate = insurexpDate;
    }

    public String getInsurImg() {
        return insurImg;
    }

    public void setInsurImg(String insurImg) {
        this.insurImg = insurImg;
    }

    public String getFcexpDate() {
        return fcexpDate;
    }

    public void setFcexpDate(String fcexpDate) {
        this.fcexpDate = fcexpDate;
    }

    public String getFcImg() {
        return fcImg;
    }

    public void setFcImg(String fcImg) {
        this.fcImg = fcImg;
    }
}
