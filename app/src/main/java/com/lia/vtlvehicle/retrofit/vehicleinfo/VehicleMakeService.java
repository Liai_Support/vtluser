package com.lia.vtlvehicle.retrofit.vehicleinfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface VehicleMakeService {
    @GET("tech.php?action=listVehiclesMake")
    Call<List<VehicleMakeAPI>> getAllVehicleMake();
}
