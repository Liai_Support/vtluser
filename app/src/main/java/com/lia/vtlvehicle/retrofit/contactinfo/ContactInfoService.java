package com.lia.vtlvehicle.retrofit.contactinfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ContactInfoService {
    @GET("contactInfo/")
    Call<List<ContactInfoAPI>> getAllContactInfo();

    @POST("tech.php?action=updateFleetOwner")
    //Call<ContactInfoAPI> insertContactInfoData(@Body JSONObject jsonObject);
    Call<ContactInfoAPI> insertContactInfoData(@Body ContactInfoAPI contactInfoAPI);

    @PUT("contactInfo/{id}")
    Call<ContactInfoAPI> updateContactInfo(@Path("id") int id, @Body ContactInfoAPI contactInfoAPI);

    @DELETE("contactInfo/{id}")
    Call<ContactInfoAPI> deleteContactInfo(@Path("id") int id);
}
