package com.lia.vtlvehicle.model;

public class Fowner {
    private int id;
    private  String fullName;
    private  String dob;
    private  String contactNo;
    private  String fpassword;
    private  String relationship;
    private  String aadhaarNo;
    private  String aadhaarImg;
    private  String pancardNo;
    private  String pancardImg;
    private  String noOfVehicleOwned;
    private  String status;
    private  String token;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getFpassword() {
        return fpassword;
    }

    public void setFpassword(String fpassword) {
        this.fpassword = fpassword;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getAadhaarImg() {
        return aadhaarImg;
    }

    public void setAadhaarImg(String aadhaarImg) {
        this.aadhaarImg = aadhaarImg;
    }

    public String getPancardNo() {
        return pancardNo;
    }

    public void setPancardNo(String pancardNo) {
        this.pancardNo = pancardNo;
    }

    public String getPancardImg() {
        return pancardImg;
    }

    public void setPancardImg(String pancardImg) {
        this.pancardImg = pancardImg;
    }

    public String getNoOfVehicleOwned() {
        return noOfVehicleOwned;
    }

    public void setNoOfVehicleOwned(String noOfVehicleOwned) {
        this.noOfVehicleOwned = noOfVehicleOwned;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
