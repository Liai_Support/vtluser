package com.lia.vtlvehicle.model;

public class CompletedTripBo {
    private int tripId;
    private String tripDate;
    private String tripFrom;
    private String tripTo;
    private String tripSal;
    private String tripVechName;
    private String tripVechType;
    private String tripVechReg;
    private String tripVecPermit;
    private String tripVecPayStatus;
    public CompletedTripBo(int tripId,String tripDate,String tripFrom,String tripTo,String tripSal,String tripVechName,String tripVechType,String tripVechReg,
                           String tripVecPermit,String tripVecPayStatus){
        this.tripId=tripId;
        this.tripDate=tripDate;
        this.tripFrom=tripFrom;
        this.tripTo=tripTo;
        this.tripSal=tripSal;
        this.tripVechName=tripVechName;
        this.tripVechType=tripVechType;
        this.tripVechReg=tripVechReg;
        this.tripVecPermit=tripVecPermit;
        this.tripVecPayStatus=tripVecPayStatus;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripFrom() {
        return tripFrom;
    }

    public void setTripFrom(String tripFrom) {
        this.tripFrom = tripFrom;
    }

    public String getTripTo() {
        return tripTo;
    }

    public void setTripTo(String tripTo) {
        this.tripTo = tripTo;
    }

    public String getTripSal() {
        return tripSal;
    }

    public void setTripSal(String tripSal) {
        this.tripSal = tripSal;
    }

    public String getTripVechName() {
        return tripVechName;
    }

    public void setTripVechName(String tripVechName) {
        this.tripVechName = tripVechName;
    }

    public String getTripVechType() {
        return tripVechType;
    }

    public void setTripVechType(String tripVechType) {
        this.tripVechType = tripVechType;
    }

    public String getTripVechReg() {
        return tripVechReg;
    }

    public void setTripVechReg(String tripVechReg) {
        this.tripVechReg = tripVechReg;
    }

    public String getTripVecPermit() {
        return tripVecPermit;
    }

    public void setTripVecPermit(String tripVecPermit) {
        this.tripVecPermit = tripVecPermit;
    }

    public String getTripVecPayStatus() {
        return tripVecPayStatus;
    }

    public void setTripVecPayStatus(String tripVecPayStatus) {
        this.tripVecPayStatus = tripVecPayStatus;
    }
}
