package com.lia.vtlvehicle.model;

public class OngoingTripBo {
    private int tripId;
    private String tripDate;
    private String tripFrom;
    private String tripTo;
    private String tripSal;
    private String tripVechName;
    private String tripVechType;
    private String tripVechReg;
    private String tripVecPermit;
    private String tripStatus;
    private String tripUnloaded;
    private String tripLoaded;
    private String triphalting;
    private String triphaltingrmrk;
    private String tripother;
    private String tripotherrmrk;
    private String tripayStatus;
    public OngoingTripBo(int tripId,String tripDate,String tripFrom,String tripTo,String tripSal,String tripVechName,String tripVechType,String tripVechReg,
                           String tripVecPermit,String tripStatus,String tripUnloaded,String tripLoaded,String triphalting,String triphaltingrmrk,
                         String tripother,String tripotherrmrk,String tripayStatus){
        this.tripId=tripId;
        this.tripDate=tripDate;
        this.tripFrom=tripFrom;
        this.tripTo=tripTo;
        this.tripSal=tripSal;
        this.tripVechName=tripVechName;
        this.tripVechType=tripVechType;
        this.tripVechReg=tripVechReg;
        this.tripVecPermit=tripVecPermit;
        this.tripStatus=tripStatus;
        this.tripUnloaded=tripUnloaded;
        this.tripLoaded=tripLoaded;
        this.triphalting=triphalting;
        this.triphaltingrmrk=triphaltingrmrk;
        this.tripother=tripother;
        this.tripotherrmrk=tripotherrmrk;
        this.tripayStatus=tripayStatus;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripFrom() {
        return tripFrom;
    }

    public void setTripFrom(String tripFrom) {
        this.tripFrom = tripFrom;
    }

    public String getTripTo() {
        return tripTo;
    }

    public void setTripTo(String tripTo) {
        this.tripTo = tripTo;
    }

    public String getTripSal() {
        return tripSal;
    }

    public void setTripSal(String tripSal) {
        this.tripSal = tripSal;
    }

    public String getTripVechName() {
        return tripVechName;
    }

    public void setTripVechName(String tripVechName) {
        this.tripVechName = tripVechName;
    }

    public String getTripVechType() {
        return tripVechType;
    }

    public void setTripVechType(String tripVechType) {
        this.tripVechType = tripVechType;
    }

    public String getTripVechReg() {
        return tripVechReg;
    }

    public void setTripVechReg(String tripVechReg) {
        this.tripVechReg = tripVechReg;
    }

    public String getTripVecPermit() {
        return tripVecPermit;
    }

    public void setTripVecPermit(String tripVecPermit) {
        this.tripVecPermit = tripVecPermit;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripUnloaded() {
        return tripUnloaded;
    }

    public void setTripUnloaded(String tripUnloaded) {
        this.tripUnloaded = tripUnloaded;
    }

    public String getTripLoaded() {
        return tripLoaded;
    }

    public void setTripLoaded(String tripLoaded) {
        this.tripLoaded = tripLoaded;
    }

    public String getTriphalting() {
        return triphalting;
    }

    public void setTriphalting(String triphalting) {
        this.triphalting = triphalting;
    }

    public String getTriphaltingrmrk() {
        return triphaltingrmrk;
    }

    public void setTriphaltingrmrk(String triphaltingrmrk) {
        this.triphaltingrmrk = triphaltingrmrk;
    }

    public String getTripother() {
        return tripother;
    }

    public void setTripother(String tripother) {
        this.tripother = tripother;
    }

    public String getTripotherrmrk() {
        return tripotherrmrk;
    }

    public void setTripotherrmrk(String tripotherrmrk) {
        this.tripotherrmrk = tripotherrmrk;
    }

    public String getTripayStatus() {
        return tripayStatus;
    }

    public void setTripayStatus(String tripayStatus) {
        this.tripayStatus = tripayStatus;
    }
}
