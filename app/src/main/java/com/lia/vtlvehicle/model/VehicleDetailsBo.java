package com.lia.vtlvehicle.model;

public class VehicleDetailsBo {
    private int id;
    private String vehicleType;
    private String vehiclemake;
    private String registNo;
    private String permiType;
    private  String insuranceExp;
    private  String fcExp;
    private  String vStatus;
    private  String details;
    public VehicleDetailsBo(int id,String vehicleType,String vehiclemake,String registNo,String insuranceExp,String fcExp,String vStatus,String permiType,String details){
        this.id=id;
        this.vehicleType=vehicleType;
        this.vehiclemake=vehiclemake;
        this.registNo=registNo;
        this.permiType=permiType;
        this.insuranceExp=insuranceExp;
        this.fcExp=fcExp;
        this.vStatus=vStatus;
        this.details=details;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehiclemake() {
        return vehiclemake;
    }

    public void setVehiclemake(String vehiclemake) {
        this.vehiclemake = vehiclemake;
    }

    public String getRegistNo() {
        return registNo;
    }

    public void setRegistNo(String registNo) {
        this.registNo = registNo;
    }

    public String getInsuranceExp() {
        return insuranceExp;
    }

    public void setInsuranceExp(String insuranceExp) {
        this.insuranceExp = insuranceExp;
    }

    public String getFcExp() {
        return fcExp;
    }

    public void setFcExp(String fcExp) {
        this.fcExp = fcExp;
    }

    public String getvStatus() {
        return vStatus;
    }

    public void setvStatus(String vStatus) {
        this.vStatus = vStatus;
    }

    public String getPermiType() {
        return permiType;
    }

    public void setPermiType(String permiType) {
        this.permiType = permiType;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
