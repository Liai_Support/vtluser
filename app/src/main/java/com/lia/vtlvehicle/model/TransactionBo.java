package com.lia.vtlvehicle.model;

public class TransactionBo {
    private int tripId;
    private String tripDate;
    private String tripMovDate;
    private String tripFrom;
    private String tripTo;
    private String tripPayType;
    private String tripTrip;
    private String tripStatus;
    private String tripHire;
    private String tripAdv;
    private String tripBal;
    private String tripHalt;
    private String tripOther;
    private String tripTotbal;
    public TransactionBo(int tripId,String tripDate,String tripMovDate,String tripFrom,String tripTo,String tripPayType,String tripTrip,String tripStatus,String tripHire,String tripAdv
            ,String tripBal,String tripHalt,String tripOther,String tripTotbal){
        this.tripId=tripId;
        this.tripDate=tripDate;
        this.tripMovDate=tripMovDate;
        this.tripFrom=tripFrom;
        this.tripTo=tripTo;
        this.tripPayType=tripPayType;
        this.tripTrip=tripTrip;
        this.tripStatus=tripStatus;
        this.tripHire=tripHire;
        this.tripAdv=tripAdv;
        this.tripBal=tripBal;
        this.tripHalt=tripHalt;
        this.tripOther=tripOther;
        this.tripTotbal=tripTotbal;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripMovDate() {
        return tripMovDate;
    }

    public void setTripMovDate(String tripMovDate) {
        this.tripMovDate = tripMovDate;
    }

    public String getTripFrom() {
        return tripFrom;
    }

    public void setTripFrom(String tripFrom) {
        this.tripFrom = tripFrom;
    }

    public String getTripTo() {
        return tripTo;
    }

    public void setTripTo(String tripTo) {
        this.tripTo = tripTo;
    }

    public String getTripPayType() {
        return tripPayType;
    }

    public void setTripPayType(String tripPayType) {
        this.tripPayType = tripPayType;
    }

    public String getTripTrip() {
        return tripTrip;
    }

    public void setTripTrip(String tripTrip) {
        this.tripTrip = tripTrip;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripHire() {
        return tripHire;
    }

    public void setTripHire(String tripHire) {
        this.tripHire = tripHire;
    }

    public String getTripAdv() {
        return tripAdv;
    }

    public void setTripAdv(String tripAdv) {
        this.tripAdv = tripAdv;
    }

    public String getTripBal() {
        return tripBal;
    }

    public void setTripBal(String tripBal) {
        this.tripBal = tripBal;
    }

    public String getTripHalt() {
        return tripHalt;
    }

    public void setTripHalt(String tripHalt) {
        this.tripHalt = tripHalt;
    }

    public String getTripOther() {
        return tripOther;
    }

    public void setTripOther(String tripOther) {
        this.tripOther = tripOther;
    }

    public String getTripTotbal() {
        return tripTotbal;
    }

    public void setTripTotbal(String tripTotbal) {
        this.tripTotbal = tripTotbal;
    }
}
